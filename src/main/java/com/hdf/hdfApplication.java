package com.hdf;

import com.hdf.controller.LoginController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class hdfApplication {
    public static void main(String[] args) {
        SpringApplication.run(hdfApplication.class,args);
        System.out.println("main after run");
    }
}
